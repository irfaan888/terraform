variable resourcegroup {
  #default     = "TestAutomationInfrastructure"
  type = string
}

variable location {
  type = string
 # default     = "eastus"
}

variable vname {
  default     = "TaskManager-server"
}

variable addressspace {
    type      = "list"
  default     = ["10.0.0.0/16"]
}

variable addressprefix {
  default     = "10.0.2.0/24"
}

variable subname {
  default     = "subnet"
}

# variable tag {
#   type      = "map"

#   default    {
#       "key" = "value"
#       "1"   = "2"
#       "tf"  = "res"
#   }
# }

variable publicip {
  default     = "publicipname"
}

variable machinename {
  default     = "TaskManager-server"
}

variable vmname {
  type = string
  #default     = "TaskManager-server"
}

variable vmsize {
  default     = "Standard_DS1_v2"
}

variable "scfile" {
  type = string
  default = "yu.bash"
}
