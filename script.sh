#!/bin/sh

sudo apt update

# sudo apt install python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools -y

#addded a comment
#install mongodb db
 wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
 echo "After wget"
 echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
 echo "After repository addition"
 sudo apt-get update
 echo "After update"
 sudo apt-get install -y mongodb-org
 echo "Done mongo"
 sudo systemctl start mongod

 echo "Starting mongod"
 sudo systemctl enable mongod

#Checking for mongo database
RESULT=$?   # returns 0 if mongo eval succeeds

if [ $RESULT -ne 0 ]; then
    echo "mongodb not running"
    exit 1
else 
    echo "mongodb running!"
fi


 sudo apt update

#Installing python environment
sudo apt install python3-venv -y

#Cloning project
sudo git clone https://gitlab.com/roshankunghadkar/taskmanager

sudo chown -R gslab /home/gslab/taskmanager

cd /home/gslab/taskmanager

python3.6 -m venv myprojectenv

source myprojectenv/bin/activate

echo "Installing Python dependencies"
 pip3 install wheel
 pip3 install flask
 pip3 install bson
 pip3 install mongoengine
 pip3 install flask-mongoengine
 pip3 install datetime
 pip3 install gunicorn

 

 #Upgrade Python
#  sudo apt install python3.8 -y

#install mongodb
#  wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
#  echo "After wget"
#  echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
#  echo "After repository addition"
#  sudo apt-get update
#  echo "After update"
#  sudo apt-get install -y mongodb-org
#  echo "Done mongo"
#  sudo systemctl start mongod

#  echo "Starting mongod"
#  sudo systemctl enable mongod

 

 sudo chown gslab /etc
 sudo chown gslab /etc/systemd
 sudo chown gslab /etc/systemd/system



#Verifying dependencies
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("flask"))'; then
    echo 'flask found'
else
    echo 'flask not found'

fi

if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("mongoengine"))'; then
    echo 'mongoengine found'
else
    echo 'mongoengine not found'
fi

if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("bson"))'; then
    echo 'bson found'
else
    echo 'bson not found'
fi

mongo --eval "db.stats()"  # do a simple harmless command of some sort

RESULT=$?   # returns 0 if mongo eval succeeds

if [ $RESULT -ne 0 ]; then
    echo "mongodb not running"
    exit 1
else 
    echo "mongodb running!"
fi

sudo touch /etc/systemd/system/task.service

#Copying service to etc
sudo cp /home/gslab/taskmanager/project1.service  /etc/systemd/system/task.service
echo "Copied Successfully!!!!"

sudo chmod 777 /etc/systemd/system/task.service
#Starting the service
sudo systemctl start task.service


 echo "Running application"
#python3 task.py
 echo "Executed!!!!!!"



 
